Cursus NMDADI
=======================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Documenten
---------------

* [Inleiding en Briefings](https://bitbucket.org/drdynscript/nmdadi_201415/src/a99a16d64dbd4364fe8aa4f1f938baa4ae281d6d/nmdadi_201415_inleiding.md?at=master "Inleiding en Briefings Cursus NMDAD-I")
* [Applicaties](https://bitbucket.org/drdynscript/nmdadi_201415/src/9728f513e192adbd7d1745134c7585ae5ef971c1/applicaties.md?at=master "Wat zijn applicaties?")
* [Markdown](https://bitbucket.org/drdynscript/nmdadi_201415/src/9728f513e192adbd7d1745134c7585ae5ef971c1/markdown.md?at=master "Markdown Syntax")
* [CSS Transformaties](https://bitbucket.org/drdynscript/nmdadi_201415/src/9728f513e192adbd7d1745134c7585ae5ef971c1/css_transformaties.md?at=master "CSS transformaties via transform eigenschap")
* [CSS Transities en Animaties](https://bitbucket.org/drdynscript/nmdadi_201415/src/7c3be2102584aaf1f7d5dfa3cab60427109940e3/css_animaties.md?at=master "CSS transities via transition eigenschap")
* [Structuur van een frontend webapplicatie](https://bitbucket.org/drdynscript/nmdadi_201415/src/c48fbf29e7770ff9a6fa50d272ee69d040222b1b/frontend_template.md?at=master "Structuur van een frontend webapplicatie")
* [Design principles in Webdesign](https://bitbucket.org/drdynscript/nmdadi_201415/src/e4405098c1306d51af507a1fdab729c5af529c4b/frontend_designprinciples.md?at=master "Design principles in Webdesign")
* [HTML5 Global Attributes](https://bitbucket.org/drdynscript/nmdadi_201415/src/202ba58ff9c8440d3b64fbda5a1912a217265e93/html5_globalattributes.md?at=master "HTML5 Global Attributes")
* [CSS Selectoren](https://bitbucket.org/drdynscript/nmdadi_201415/src/e4405098c1306d51af507a1fdab729c5af529c4b/css_selectoren.md?at=master "CSS Selectoren")
* [CSS 3D](https://bitbucket.org/drdynscript/nmdadi_201415/src/e4405098c1306d51af507a1fdab729c5af529c4b/css_3d.md?at=master "CSS 3D")
* [Data Formats](https://bitbucket.org/drdynscript/nmdadi_201415/src/29df8b56608cecb371496744606cc8026a085807/data_formats.md?at=master "Data Formats")
* [Data Consuming](https://bitbucket.org/drdynscript/nmdadi_201415/src/29df8b56608cecb371496744606cc8026a085807/data_consuming.md?at=master "Data Consuming")
* [JavaScript Inleiding](https://bitbucket.org/drdynscript/nmdadi_201415/src/29df8b56608cecb371496744606cc8026a085807/js_introduction.md?at=master "JavaScript Inleinding")
* [JavaScript Routing](https://bitbucket.org/drdynscript/nmdadi_201415/src/29df8b56608cecb371496744606cc8026a085807/js_routing.md?at=master "JavaScript Routing")
* [Frontend Automation](https://bitbucket.org/drdynscript/nmdadi_201415/src/3fd64b31d488eb4f4b8336a35a53b822c7eb2311/frontend_automationtools.md?at=master "Frontend Automation")



Downloads
-------------

- Je kan het wolkje in BitBucket aanklikken om al het cursusmateriaal te downloaden
- Je kan ook eerst het cursusmateriaal lokaal klonen: `git clone https://drdynscript@bitbucket.org/drdynscript/nmdadi_201415.git "naamdirectory"`
- Daarna ga je de wijzigingen lokaal binnentrekken: `git pull -u origin master`. Je moet dan wel in de aangemaakte folder "naamdirectory" zitten!

Makers
--------
**Philippe De Pauw - Waterschoot**

* <http://twitter.com/drdynscript>
* <http://bitbucket.org/drdynscript>
* <https://www.linkedin.com/pub/philippe-de-pauw/6/a33/5a5>
* <philippe.depauw@arteveldehs.be>

Arteveldehogeschool
-------------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>


Copyright and license
--------------------------

Code en documentatie copyright 2003-2015 Arteveldehogeschool | Opleiding Grafische en Digitale Media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de   Creative Commons licentie.